<?php

/**
 * Orange Theme
 * © Get.Noticed 2019
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/GetNoticed/orange', __DIR__);
