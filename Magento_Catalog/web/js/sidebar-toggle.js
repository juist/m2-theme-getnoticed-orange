define([
  'jquery'
], function ($) {
  return function(configuration, element) {
    var $sidebar = $('.sidebar-main');
    var $sidebarClose = $('.sidebar-toggle-close');

    $(element).on('click', function(){
      $sidebar.toggleClass('sidebar-open');
    });
    $($sidebarClose).on('click', function () {
      $sidebar.toggleClass('sidebar-open');
    })
  }
});