define([
  'jquery',
  'owlCarousel'
], function($){
  return function(configuration, element) {
    $(element).owlCarousel({
      items: 1,
      stagePadding: 0,
      margin: 0,
      loop: true,
      singleItem: true,
      autoplay: true,
      autoplayTimeout: 8000,
      dots: true,
      nav: false,
      responsive: {
        0: {
          dots: false,
        },
        768: {
          dots: true,
        },
      }
    });
  }
});