/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
  'jquery'
], function ($) {
  /**
   * Shrinking header with debounce
   */
  var header = $('.page-header');
  var navigation = $('.nav-sections');
  var page = $('.page-wrapper');
  var shrinkHeader = header.outerHeight() + navigation.outerHeight();
  
  var headerJs = document.querySelector('.page-header');
  var checkStyle = getComputedStyle(headerJs).getPropertyValue('position');
  
  if (checkStyle === "fixed") {
    // If scripts get to heavy, use debounce
    // $(window).scroll(debounce(function () {
    $(window).scroll(function () {
      var scroll = getCurrentScroll();
      if (scroll >= shrinkHeader) {
        header.addClass('shrink');
        navigation.addClass('shrink');
      } else {
        header.removeClass('shrink');
        navigation.removeClass('shrink');
      }
    });
    // }, 50));

    function getCurrentScroll() {
      return window.pageYOffset || document.documentElement.scrollTop;
    }

    function debounce(func, delay) {
      var inDebounce;
      return function () {
        var context = this,
          args = arguments;
        clearTimeout(inDebounce);
        inDebounce = setTimeout(function () {
          return func.apply(context, args);
        }, delay);
      };
    }
  }
});